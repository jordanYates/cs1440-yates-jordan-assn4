import os

class config:
    cfg = {}

    def __init__(self, fileName, gradName):
        fileDir = os.path.dirname(os.path.realpath('__file__'))
        foundFile = os.path.join(fileDir, f'../data/{fileName}')
        foundFile = os.path.abspath(os.path.realpath(foundFile))

        with open(foundFile) as f:
            for line in f:
                (key, val) = line.split()
                key = key[:-1]
                key = key.lower()
                if(key == 'type'):
                    val = val.lower()
                    self.cfg[key] = val
                elif(key == 'iterations' or key == 'pixels'):
                    self.cfg[key] = int(val)
                else:
                    self.cfg[key] = float(val)

        # Mandelbrot prep
        if ('creal' not in self.cfg.keys()):
            self.cfg['creal'] = 0
        if ('cimag' not in self.cfg.keys()):
            self.cfg['cimag'] = 0

        self.cfg['gradName'] = gradName

    def getCFG(self):
        return self.cfg


# Get some ideas from https://atopon.org/mandel/
# or https://sciencedemos.org.uk/mandelbrot.php

# TODO: write a small helper program to convert the websites'
#        (minX, minY), (maxX, maxY) coordinates into my
#        (centerX, centerY) + axisLength scheme
# def converter(minX, minY, maxX, maxY):
#     centerX = (minX + maxX)/2
#     centerY = (minY + maxY)/2
#     axisLength = maxX - minX
# I'm not sure how to fully implement this until we are add functionality that can take min and max x and y. This is
# the bare bones functions in it that will be called if needed.
