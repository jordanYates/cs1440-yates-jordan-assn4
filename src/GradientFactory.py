from Gradient import Gradient
from DefaultGrad import DefaultGrad
from GreyScale import GreyScale
from Rainbow import Rainbow
from RedToGreen import RedToGreen
from BlueToOrange import BlueToOrange
from Lightning import Lightning


class GradientFactory:
    grad = Gradient

    def __init__(self, iterations, gradName):
        if(gradName == 'default'):
            self.grad = DefaultGrad(iterations)

        if(gradName == 'greyScale'):
            self.grad = GreyScale(iterations)

        if(gradName == 'rainbow'):
            self.grad = Rainbow(iterations)

        if(gradName == 'redToGreen'):
            self.grad = RedToGreen(iterations)

        if(gradName == 'blueToOrange'):
            self.grad = BlueToOrange(iterations)

        if(gradName == 'lightning'):
            self.grad = Lightning(iterations)

    def getGrad(self):
        return self.grad.getGrad()