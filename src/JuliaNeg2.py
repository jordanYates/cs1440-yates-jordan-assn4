from Fractal import Fractal


class JuliaNeg2(Fractal):
    iterations = 0

    def __init__(self, i):
        self.iterations = i

    def count(self, z, creal, cimag):
        c = complex(creal, cimag)

        for i in range(self.iterations):
            if(z != 0):
                z = 1/(z * z) + c
                if abs(z) > 2:
                    return i
        return self.iterations - 1