from Gradient import Gradient


class Lightning(Gradient):
    def __init__(self, iterations):
        self.grad = [None] * iterations
        # Black
        black = [0, 0, 0]
        #
        blue = [0, 255, 255]
        # White
        white = [255, 255, 255]

        subIterations = int(iterations / 4)

        blToBu = self.gradBuilder(black, blue, subIterations + 1)
        buToWh = self.gradBuilder(blue, white, subIterations + 1)
        whToBu = self.gradBuilder(white, blue, subIterations)
        buToBl = self.gradBuilder(blue, black, subIterations)


        self.grad = blToBu + buToWh + whToBu + buToBl