from Gradient import Gradient


class Rainbow(Gradient):
    def __init__(self, iterations):
        self.grad = [None] * iterations
        # Black
        start = [0, 0, 0]
        # Purple
        purple = [128, 0, 128]
        # Blue
        blue = [0, 0, 255]
        # Green
        green = [0, 255, 0]
        # Yellow
        yellow = [255, 255, 0]
        # Orange
        orange = [255, 165, 0]
        # Red
        red = [255, 0, 0]
        # White
        stop = [255, 255, 255]

        subIterations = int(iterations / 7)

        blToPu = self.gradBuilder(start, purple, subIterations + 1)
        puToBu = self.gradBuilder(purple, blue, subIterations + 1)
        buToGr = self.gradBuilder(blue, green, subIterations + 1)
        grToYe = self.gradBuilder(green, yellow, subIterations)
        yeToOr = self.gradBuilder(yellow, orange, subIterations)
        orToRe = self.gradBuilder(orange, red, subIterations)
        reToBl = self.gradBuilder(red, stop, subIterations)

        self.grad = blToPu + puToBu + buToGr + grToYe + yeToOr + orToRe + reToBl
