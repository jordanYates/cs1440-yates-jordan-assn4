from Gradient import Gradient


class RedToGreen(Gradient):
    def __init__(self, iterations):
        self.grad = [None] * iterations
        # Set start color
        start = [106, 17, 17]
        # Set end color
        stop = [45, 172, 58]
        self.grad = self.gradBuilder(start, stop, iterations)