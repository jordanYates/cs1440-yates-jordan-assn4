from Fractal import Fractal


class Mandelbrot(Fractal):
    iterations = 0

    def __init__(self, i):
        self.iterations = i

    def count(self, c, creal, cimag):
        z = complex(creal, cimag)

        for i in range(self.iterations):
            z = z * z + c
            if abs(z) > 2:
                return i
        return self.iterations - 1