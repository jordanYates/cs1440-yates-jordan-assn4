from Fractal import Fractal


class BurningShip(Fractal):
    iterations = 0

    def __init__(self, i):
        self.iterations = i

    def count(self, z, creal, cimag):
        c = complex(creal, cimag)

        for i in range(self.iterations):
            z = (abs(z.real) + abs(z.imag) * 1j) * (abs(z.real) + abs(z.imag) * 1j) + c
            if abs(z) > 2:
                return i
        return self.iterations - 1