from Fractal import Fractal
from Mandelbrot import Mandelbrot
from Julia import Julia
from BurningShip import BurningShip
from Julia6 import Julia6
from JuliaNeg2 import JuliaNeg2


class FractalFactory:
    myFrac = Fractal

    def __init__(self, type, iterations):
        if(type == 'mandelbrot'):
            self.myFrac = Mandelbrot(iterations)

        if(type == 'julia'):
            self.myFrac = Julia(iterations)

        if(type == 'burningship'):
            self.myFrac = BurningShip(iterations)

        if(type == 'julia6'):
            self.myFrac = Julia6(iterations)

        if (type == 'julianeg2'):
            self.myFrac = JuliaNeg2(iterations)


    def getFrac(self):
        return self.myFrac