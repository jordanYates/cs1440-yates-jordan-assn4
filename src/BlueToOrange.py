from Gradient import Gradient


class BlueToOrange(Gradient):
    def __init__(self, iterations):
        self.grad = [None] * iterations
        # Set start color
        start = [2, 0, 122]
        # Set end color
        stop = [239, 161, 4]
        self.grad = self.gradBuilder(start, stop, iterations)
