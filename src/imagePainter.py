import sys
from GradientFactory import GradientFactory
from FractalFactory import FractalFactory
from tkinter import Canvas


def minMaxPix(centerx, centery, axislength, pix):
    # Figure out how the boundaries of the PhotoImage relate to coordinates on
    # the imaginary plane.
    minx = centerx - (axislength / 2.0)
    maxx = centerx + (axislength / 2.0)
    miny = centery - (axislength / 2.0)
    maxy = centery + (axislength / 2.0)

    # At this scale, how much length and height on the imaginary plane does one
    # pixel take?
    pixelsize = abs(maxx - minx) / pix

    return [minx, miny, pixelsize]


def paint(D, imagename, window, img):
    mmp = minMaxPix(D['centerx'], D['centery'], D['axislength'], D['pixels'])
    gradMaker = GradientFactory(D['iterations'], D['gradName'])
    myGrad = gradMaker.getGrad()

    portion = int(D['pixels'] / 64)

    fracMaker = FractalFactory(D['type'], D['iterations'])
    myFrac = fracMaker.getFrac()

    for col in range(D['pixels']):
        if col % portion == 0:
            # Update the status bar each time we complete 1/64th of the rows
            pips = int(col / portion)
            pct = col / D['pixels']
            print(f"{imagename} ({D.get('pixels')}x{D.get('pixels')}) {'=' * pips}{'_' * (64 - pips)} {pct:.0%}", file=sys.stderr)
        for row in range(D['pixels']):
            x = mmp[0] + col * mmp[2]
            y = mmp[1] + row * mmp[2]

            colorIndex = myFrac.count(complex(x, y), D['creal'], D['cimag'])
            color = myGrad[colorIndex]
            img.put(color, (col, row))

    print(f"{imagename} ({D['pixels']}x{D['pixels']}) ================================================================ 100%",
          file=sys.stderr)

    # Display the image on the screen
    canvas = Canvas(window, width=D['pixels'], height=D['pixels'], bg='#000000')
    canvas.pack()
    canvas.create_image((D['pixels']/2, D['pixels']/2), image=img, state="normal")
