from Gradient import Gradient


class GreyScale(Gradient):
    def __init__(self, iterations):
        self.grad = [None] * iterations
        # Set start color
        start = [0, 0, 0]
        # Set end color
        stop = [255, 255, 255]
        self.grad = self.gradBuilder(start, stop, iterations)