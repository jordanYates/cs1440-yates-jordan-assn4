from config import config
import imageBuilder
import sys

if(len(sys.argv) < 2):
    print("$ python main.py")
    print("Please specify a fractal configuration file!")
    print("Usage: main.py CONFIG.frac")
else:
    filename = sys.argv[1]
    if(len(sys.argv) == 3):
        gradName = sys.argv[2]
    else:
        gradName = 'default'
    con = config(filename, gradName)
    imageBuilder.imageBuilder(con.cfg, filename)





