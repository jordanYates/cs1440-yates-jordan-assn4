from tkinter import Tk, PhotoImage, mainloop
import imagePainter

def imageBuilder(cfg, filename):
    window = Tk()
    img = PhotoImage(width=cfg['pixels'], height=cfg['pixels'])
    imagePainter.paint(cfg, filename, window, img)

    img.write(filename + ".png")
    print(f"Wrote image {filename}.png")
    mainloop()
