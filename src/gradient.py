class Gradient:
    grad = []

    def gradBuilder(self, start, stop, iterations):
        tempGrad = [None] * iterations
        delta = [0.0, 0.0, 0.0]
        delta[0] = (stop[0] - start[0]) / (iterations - 1)
        delta[1] = (stop[1] - start[1]) / (iterations - 1)
        delta[2] = (stop[2] - start[2]) / (iterations - 1)
        step = start
        tempGrad[0] = self.rgbToHex(start)
        for i in range(1, iterations):
            step[0] = step[0] + delta[0]
            step[1] = step[1] + delta[1]
            step[2] = step[2] + delta[2]
            # Limiters. I don't know that they are needed, but just in case.
            if(step[0] > 255):
                step[0] = 255
            if (step[1] > 255):
                step[1] = 255
            if (step[2] > 255):
                step[2] = 255
            if (step[0] < 0):
                step[0] = 0
            if (step[1] < 0):
                step[1] = 0
            if (step[2] < 0):
                step[2] = 0
            tempGrad[i] = self.rgbToHex(step)
        return tempGrad

    def rgbToHex(self, RGB):
        # Convert rgb colors into hex colors
        RGB = [int(x) for x in RGB]
        return "#" + "".join(["0{0:x}".format(v) if v < 16 else
                              "{0:x}".format(v) for v in RGB])

    def getGrad(self):
        return self.grad
